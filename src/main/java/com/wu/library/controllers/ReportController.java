package com.wu.library.controllers;

import com.wu.library.models.Book;
import com.wu.library.models.BookReport;
import com.wu.library.repositories.admin.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("admin/report")
public class ReportController
{

    @Autowired
    private BookRepository bookRepository;

    @GetMapping("/book")
    public String bookReport(@ModelAttribute BookReport bookReport, ModelMap modelMap){
        String startDate;
        String endDate;
        Integer endDay = Integer.parseInt(bookReport.getEndDay()) +1;
        startDate = bookReport.getStartYear()+"-"+bookReport.getEndMonth()+"-"+bookReport.getEndDay();
        endDate = bookReport.getEndYear()+"-"+bookReport.getEndMonth()+"-"+endDay;
        List<Book> books = this.bookRepository.getBooksByDate(startDate,endDate);
        modelMap.addAttribute("books",books);
        return "admin/Report/bookReport";
    }

}
